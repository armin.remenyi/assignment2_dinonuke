﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMove : MonoBehaviour
{
    public Transform Player;
    public int MoveSpeed = 4;
    public int MaxDistance = 10;
    public int MinDistance = 5;
    public float health = 50f;


    GameObject target;

    private spawner _spawnManager;

    // Start is called before the first frame update
    void Start()
    {
        //target = GameObject.Find("Player");
        //_spawnManager = GameObject.FindGameObjectsWithTag("SpawnManager").GetComponent<spawner>();
    }

    // Update is called once per frame
    void Update()
    { 
        transform.LookAt(Player);

        if (Vector3.Distance(transform.position, Player.position) >= MinDistance)
        {

            transform.position += transform.forward * MoveSpeed * Time.deltaTime;

            if (Vector3.Distance(transform.position, Player.position) <= MaxDistance)
            {

            }
        }
    }

    public void TakeDamage(float amount)
    {
        health -= amount;
        if(health <= 0f)
        {
            totalScore.scoreValue += 5;
            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }


    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Finish")
        {
            villageHealth.maxHealth -= 1;
            Destroy(gameObject);

        }
    }

}
