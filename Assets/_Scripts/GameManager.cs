﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject gameOver, restart;

    // Start is called before the first frame update
    void Start()
    {
        
        gameOver.gameObject.SetActive(false);
        restart.gameObject.SetActive(false);
    }
}
