﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageInfo
{
    public int amount;
}


public class baseCombat : MonoBehaviour
{
    protected int hitPoint = 10;
    [SerializeField]
    protected int maxHitPoint = 10;
    public int HitPoint { set { hitPoint = value; } get{return hitPoint;}}
    public int HitPoints{ set { maxHitPoint = value; } get{ return maxHitPoint; }}

    private void Start()
    {
        InitCombat();
    }

    public virtual void InitCombat()
    {
        hitPoint = maxHitPoint;
    }
    public virtual void OnDamage(DamageInfo dmg)
    {
        HitPoint -= dmg.amount;

        if (HitPoint <= 0)
            OnDeath();
    }
    public virtual void OnDeath()
    {

    }

}
