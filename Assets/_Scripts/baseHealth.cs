﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class baseHealth : MonoBehaviour
{
    public int startingHealth = 50;
    public int currentHealth;
    public SliderJoint2D healthSlider;
    public Image damageImage;
    public float flashSpeed = 5f;
    public Color flashColor = new Color(1f, 0f, 0f, 0.1f);

    bool isDestroyed;
    bool damaged;

    void Awake()
    {
        currentHealth = startingHealth;
    }

    void Update()
    {
        if (damaged)
        {
            damageImage.color = flashColor;
        }
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;
    }

    public void DamageTaken (int amount)
    {
        damaged = true;
        currentHealth -= amount;
        //healthSlider.value = currentHealth;

        if(currentHealth <= 0 && !isDestroyed)
        {
            gameOver();
        }
    }

    void gameOver()
    {
        isDestroyed = true;
    }
}
