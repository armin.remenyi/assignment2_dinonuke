﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class defeatZone : MonoBehaviour
{
    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Enemy")
        {
            //levelManager.Instance.EnemyCrossed();
            Destroy(col.gameObject);
        }
    }
}
