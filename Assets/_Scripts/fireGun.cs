﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireGun : MonoBehaviour
{
    private LineRenderer laser;

    private void Start()
    {
        laser = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        laser.SetPosition(0, transform.position);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            if (hit.collider)
            {
                laser.SetPosition(1, hit.point);
            }
        }
        else laser.SetPosition(1, transform.forward * 5000);
    }


    //public float shootRate;
    //private float shootRateTimeStamp;

    //public GameObject shotPrefab;

    //RaycastHit hit;
    //float range = 100f;

    //// Update is called once per frame
    //void Update()
    //{
    //    if (Input.GetMouseButton(0))
    //    {
    //        if(Time.time > shootRateTimeStamp)
    //        {
    //            shootRateTimeStamp = Time.time + shootRate;
    //        }
    //    }
    //}

    //void shootray()
    //{
    //    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //    if (Physics.Raycast(ray, out hit, range))
    //    {
    //        GameObject laser = GameObject.Instantiate(shotPrefab, transform.position, transform.rotation) as GameObject;
    //        //laser.GetComponent<ShotBehavior>().setTarget(hit.point);
    //        GameObject.Destroy(laser, 2f);

    //    }
    //}
}
