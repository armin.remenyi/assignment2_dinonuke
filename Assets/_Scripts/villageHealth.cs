﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class villageHealth : MonoBehaviour
{
    public static int maxHealth = 30;
    public int currentHealth;
    public GameObject gameOver, restart;
    public healthBar HealthBar;

    //public static int villageHealth;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        HealthBar.SetMaxHealth(maxHealth);
        gameOver.gameObject.SetActive(false);
        restart.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Time.timeScale = 0;
            gameOver.gameObject.SetActive(true);
            restart.gameObject.SetActive(true);

        }
        HealthBar.SetHealth(maxHealth);

    }

    //private void OnCollisonEnter(Collision collider)
    //{
    //    if (collider.gameObject.tag == "Enemy")
    //    {
    //        TakeDamage(1);
    //        Destroy(collider.gameObject);

    //    }
    //}

    //void TakeDamage(int damage)
    //{
    //    currentHealth -= damage;

    //}
}
